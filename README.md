# Powiększony nagłówek
*Lorem ipsum dolor sit amet,* **consectetur adipiscing elit,** ~~sed do eiusmod tempor incididunt ut labore~~ et dolore magna aliqua. 

> Morbi leo urna molestie at elementum eu facilisis sed. Diam maecenas sed enim ut sem viverra. 

Fames ac turpis egestas sed tempus urna et. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Id ornare arcu odio ut. 

Zagnieżdżoną listę numeryczną
1. lorem
* 2. ipsum
3. dolorosa

Zagnieżdżoną listę nienumeryczną
- lorem
    - super
        - ipsum
        - oooo
- ipsum

Blok kodu programu, który ma co najmniej 3 linie
~~~
x = int(input(''))
for i in range(x):
    print('lorem')
~~~
Kod programu `x=30` zagnieżdżony w tekście

![./valley.jpeg](./valley.jpeg)